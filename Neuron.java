package com.machinelearning;


import com.ml.ArtificialNeuron.ArtificialNeuron ;
import java.io.* ;

public class Neuron {
    public static void main(String args[])throws IOException {
        ArtificialNeuron ann = new ArtificialNeuron();
        double inputs[] = {0,1,1,0};
        double weights[]= {0.11657834,0.689347,0.0423451,0.97546783};
        
        double DesiredOutPut = 2;
        int epoch=0 ;
        double LearningRate = 5.0 ;
        double ActualOutput = 0.0;
		double THETA = 2 ;
        while(epoch<1000000){
            System.out.print("TrainingCycle: "+epoch+1);
            ActualOutput = ann.createNeuron(inputs, weights,THETA);
            System.out.print("  Output got: "+ActualOutput);
            for(int k=0;k<4;k++){

                weights[k] += (-1*LearningRate*(ActualOutput-DesiredOutPut)*inputs[k]*ActualOutput*(1-ActualOutput));
            }
            //ActualOutput = ann.createNeuron(inputs, weights); 
            //System.out.println("   Output: "+ActualOutput);
         epoch++; 
         System.out.println();
        }
        System.out.println("Finally output : "+ActualOutput);
        for(int z=0;z<4;z++)
        	System.out.print(weights[z]+", ");




    }
}